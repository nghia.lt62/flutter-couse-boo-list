import 'package:flutter/material.dart';
import 'package:flutter_boo_box/ui/boo_list.dart';

void main() async {
  runApp(new BooBoxApp());
}

class BooBoxApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Flutter Demo",
      theme: new ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.deepOrange,
        fontFamily: 'MissFajardose',
      ),
      home:
//      Padding(
//        padding: const EdgeInsets.all(8.0),
//        child:
          Center(child: new BooList()),
//      ),
    );
  }
}
