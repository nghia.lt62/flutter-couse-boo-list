import 'package:flutter_boo_box/models/dog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boo_box/ui/cat_details/header/dog_colored_image.dart';
import 'package:meta/meta.dart';

class DogDetailHeader extends StatefulWidget {
  final Dog dog;
  final Object avatarTag;

  DogDetailHeader(
    this.dog, {
    @required this.avatarTag,
  });

  @override
  _DogDetailHeaderState createState() {
    return new _DogDetailHeaderState();
  }
}

class _DogDetailHeaderState extends State<DogDetailHeader> {
  static const BACKGROUND_IMAGE = 'images/bg.jpg';

  @override
  Widget build(BuildContext context) {
//    var theme = Theme.of(context);

    var screenWidth = MediaQuery.of(context).size.width;

    var diagonalBackground = new DiagonallyCutColoredImage(
      new Image.asset(
        BACKGROUND_IMAGE,
        width: screenWidth,
        height: 280.0,
        fit: BoxFit.cover,
      ),
      color: const Color(0xBB42A5F5),
    );

    var avatar = new Hero(
      tag: widget.avatarTag,
      child: new CircleAvatar(
        backgroundImage: new NetworkImage(widget.dog.imageUrl),
        radius: 75.0,
      ),
    );

    var likeInfo = new Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          new Icon(
            Icons.thumb_up,
            color: Colors.white,
            size: 16.0,
          ),
          new Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: new Text(
                widget.dog.likeCounter.toString(),
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontFamily: 'Roboto'),
              ))
        ],
      ),
    );

    var actionButtons = new Padding(
      padding: const EdgeInsets.only(
        top: 16.0,
        left: 16.0,
        right: 16.0,
      ),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          new ClipRRect(
            borderRadius: new BorderRadius.circular(24.0),
            child: new MaterialButton(
                minWidth: 140.0,
                color: Colors.deepOrange,
                textColor: Colors.white,
                onPressed: () async {
                  //TODO Handle Adopt
                },
                child: new Text(
                  'ADOPT ME',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black54,
                      fontFamily: 'Roboto'),
                )),
          ),
          new ClipRRect(
            borderRadius: new BorderRadius.circular(24.0),
            child: new RaisedButton(
              color: Colors.lightGreen,
              disabledColor: Colors.grey,
              textColor: Colors.white,
              onPressed: () async {
                //TODO Handle Like
              },
              child: new Text(
                'LIKE',
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black54,
                    fontFamily: 'Roboto'),
              ),
            ),
          ),
        ],
      ),
    );

    return new Stack(
      children: [
        diagonalBackground,
        new Align(
          alignment: FractionalOffset.bottomCenter,
          heightFactor: 1.4,
          child: new Column(
            children: [
              avatar,
              likeInfo,
              actionButtons,
            ],
          ),
        ),
        new Positioned(
          top: 26.0,
          left: 4.0,
          child: new BackButton(color: Colors.white),
        ),
      ],
    );
  }
}
