import 'package:flutter_boo_box/models/dog.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:flutter_boo_box/ui/cat_details/header/details_header.dart';

class DogDetailPage extends StatefulWidget {
  final Dog dog;
  final Object avatarTag;

  DogDetailPage(this.dog, {@required this.avatarTag});

  @override
  _DogDetailsPageState createState() {
    return new _DogDetailsPageState();
  }
}

class _DogDetailsPageState extends State<DogDetailPage> {
  var linearGradient = new BoxDecoration(
      gradient: new LinearGradient(
          begin: FractionalOffset.centerRight,
          end: FractionalOffset.bottomLeft,
          colors: [Colors.cyan, Colors.deepOrangeAccent]));

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new SingleChildScrollView(
        child: new Container(
          decoration: linearGradient,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new DogDetailHeader(widget.dog, avatarTag: widget.avatarTag)
            ],
          ),
        ),
      ),
    );
  }
}
