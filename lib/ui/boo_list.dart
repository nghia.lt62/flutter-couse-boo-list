import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_boo_box/services/dog_api.dart';
import 'package:flutter_boo_box/models/dog.dart';
import 'package:flutter_boo_box/ui/cat_details/details_page.dart';
import 'package:flutter_boo_box/utils/routes.dart';

class BooList extends StatefulWidget {
  @override
  _BooListState createState() => new _BooListState();
}

class _BooListState extends State<BooList> {
  List<Dog> _dogs = [];

  @override
  void initState() {
    super.initState();
    _loadBoo();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.blue,
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return new Container(
      margin: const EdgeInsets.fromLTRB(
          8.0, // A left margin of 8.0
          56.0, // A top margin of 56.0
          8.0, // A right margin of 8.0
          0.0 // A bottom margin of 0.0
          ),
      child: new Column(
        children: <Widget>[
          _getAppTitleWidget(),
          _getAppListViewWidget(),
        ],
      ),
    );
  }

  Widget _getAppTitleWidget() {
    return new Padding(
      padding: EdgeInsets.only(left: 16.0, top: 32.0),
      child: new Text('Boo List',
          style: new TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 64.0,
          )),
    );
  }

  Future<Null> refresh() {
    _loadBoo();
    return new Future<Null>.value();
  }

  Widget _getAppListViewWidget() {
    return new Flexible(
        child: new RefreshIndicator(
            onRefresh: refresh,
            child: new ListView.builder(
                physics: const AlwaysScrollableScrollPhysics(),
                itemCount: _dogs.length,
                itemBuilder: _buildCatItem)));
  }

  Widget _buildCatItem(BuildContext context, int index) {
    Dog dog = _dogs[index];
    return new Container(
//      margin: const EdgeInsets.only(top: 5.0),
      child: new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new ListTile(
              onTap: () => _navigateToDogDetails(dog, index),
              leading: new Hero(
                tag: index,
                child: new CircleAvatar(
                  backgroundImage: new NetworkImage(dog.imageUrl),
                  // child: new FadeInImage.memoryNetwork(
                  //   placeholder: kTransparentImage,
                  //   image: dog.imageUrl,
                  // ),
                ),
              ),
              title: new Text(
                dog.name,
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black54,
                    fontFamily: 'Roboto'),
              ),
              subtitle: new Text(
                dog.description,
                style: new TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.black54,
                    fontFamily: 'Roboto'),
              ),
              isThreeLine: true,
              dense: false,
            ),
          ],
        ),
      ),
    );
  }

  _loadBoo() async {
    String fileData =
        await DefaultAssetBundle.of(context).loadString("assets/dogs.json");
    // for (Dog dog in DogApi.allDogFromJson(fileData)) {
    //     _dogs.add(dog);
    //     print(dog);
    // }
    setState(() {
      //will nofify the framework that _dogs has changed when we load the dogs in asynchronously
      _dogs = DogApi.allDogFromJson(fileData);
    });
  }

  _navigateToDogDetails(Dog dog, Object avatarDog) {
    Navigator.of(context).push(new FadePageRoute(
        builder: (c) {
          return new DogDetailPage(dog, avatarTag: avatarDog);
        },
        settings: new RouteSettings()));
  }
}
