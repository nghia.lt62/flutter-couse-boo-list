import 'dart:convert';
import 'package:flutter_boo_box/models/dog.dart';

class DogApi {
  static List<Dog> allDogFromJson(String jsonData) {
    List<Dog> dogs = [];
    json.decode(jsonData)['dogs'].forEach((dog) => dogs.add(_fromMap(dog)));
    return dogs;
  }

  static Dog _fromMap(Map<String, dynamic> map) {
    return new Dog(
      externalId: map['id'],
      name: map['name'],
      description: map['description'],
      imageUrl: map['image_url'],
      location: map['location'],
      likeCounter: map['like_counter'],
      isAdopted: map['adopted'],
      pictures: new List<String>.from(map['pictures']),
      attributes: new List<String>.from(map['cattributes']),
    );
  }
}
