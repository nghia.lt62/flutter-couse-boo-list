import 'package:meta/meta.dart';

class Dog {

  int externalId;
  String name;
  bool isAdopted;
  List<String> pictures;
  int likeCounter;
  String location;
  List<String> attributes;
  String description;
  String imageUrl;

  Dog({
    @required this.name,
    @required this.isAdopted,
    @required this.externalId,
    @required this.pictures,
    @required this.likeCounter,
    @required this.location,
    @required this.attributes,
    @required this.description,
    @required this.imageUrl,
  });

  @override
  String toString() {
    return "Dog $externalId is named $name";
  }
}
